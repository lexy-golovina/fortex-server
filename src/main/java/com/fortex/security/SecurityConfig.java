package com.fortex.security;

import com.fortex.entities.RoleType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${app.host}")
    private Set<String> appHosts;

    private final FortexAuthenticationSuccessHandler fortexAuthenticationSuccessHandler;
    private final FortexAuthenticationFailureHandler fortexAuthenticationFailureHandler;

    public SecurityConfig(FortexAuthenticationSuccessHandler fortexAuthenticationSuccessHandler, FortexAuthenticationFailureHandler fortexAuthenticationFailureHandler) {
        this.fortexAuthenticationSuccessHandler = fortexAuthenticationSuccessHandler;
        this.fortexAuthenticationFailureHandler = fortexAuthenticationFailureHandler;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(new ArrayList<>(appHosts));
        configuration.setAllowedHeaders(Arrays.asList(HttpHeaders.ACCEPT, HttpHeaders.CONTENT_TYPE, HttpHeaders.AUTHORIZATION));
        configuration.setAllowedMethods(Arrays.asList(HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.PUT.name(), HttpMethod.DELETE.name()));
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and()
                .csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/admin/**").hasRole(RoleType.ADMIN.name())
                    .anyRequest().permitAll()
                .and()
                    .formLogin()
                        .loginProcessingUrl("/login")
                        .successHandler(fortexAuthenticationSuccessHandler)
                        .failureHandler(fortexAuthenticationFailureHandler);
    }
}
