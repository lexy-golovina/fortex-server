package com.fortex.entities;

public enum FilterType {
    DROP_DOWN, RANGE;

    private FilterType type;

    public String getType() {
        return type.name();
    }

    public void setType(String type) {
        this.type = FilterType.valueOf(type);
    }
}
