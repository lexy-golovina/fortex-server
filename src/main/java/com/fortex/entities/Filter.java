package com.fortex.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Filter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Enumerated(EnumType.STRING)
    private FilterType type;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filter")
    @JsonManagedReference
    private List<DropDownFilterValue> dropDownFilterValues = new ArrayList<>();
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "filter")
    @JsonManagedReference
    private RangeFilterValue rangeFilterValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FilterType getType() {
        return type;
    }

    public void setType(FilterType type) {
        this.type = type;
    }

    public List<DropDownFilterValue> getDropDownFilterValues() {
        return dropDownFilterValues;
    }

    public void setDropDownFilterValues(List<DropDownFilterValue> dropDownFilterValues) {
        this.dropDownFilterValues = dropDownFilterValues;
    }

    public RangeFilterValue getRangeFilterValue() {
        return rangeFilterValue;
    }

    public void setRangeFilterValue(RangeFilterValue rangeFilterValue) {
        this.rangeFilterValue = rangeFilterValue;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", dropDownFilterValues=" + dropDownFilterValues +
                ", rangeFilterValue=" + rangeFilterValue +
                '}';
    }
}
