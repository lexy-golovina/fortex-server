package com.fortex.entities;

public enum RoleType {
    ADMIN, USER;

    private RoleType roleType;

    public String getRoleType() {
        return roleType.name();
    }

    public void setRoleType(String roleType) {
        this.roleType = RoleType.valueOf(roleType);
    }
}
