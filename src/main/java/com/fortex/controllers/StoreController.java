package com.fortex.controllers;

import com.fortex.entities.StoreInfo;
import com.fortex.services.StoreService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.fortex.constants.Constants.STORE_ID;

@RestController
@RequestMapping(("/storeInfo"))
public class StoreController {

    private final StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping
    @CrossOrigin(origins = "*")
    public StoreInfo getStoreInfo() {
        return storeService.getStoreInfo(STORE_ID).orElse(new StoreInfo()); //TODO: throw 404
    }

    @PutMapping
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public StoreInfo updateStoreInfo(@RequestBody StoreInfo storeInfo) {
        return storeService.save(storeInfo);
    }
}
