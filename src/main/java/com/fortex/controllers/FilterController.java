package com.fortex.controllers;

import com.fortex.entities.Filter;
import com.fortex.services.FilterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filters")
public class FilterController {

    private static final Logger LOG = LogManager.getLogger(FilterController.class);

    private final FilterService filterService;

    public FilterController(FilterService filterService) {
        this.filterService = filterService;
    }

    @GetMapping
    @CrossOrigin(origins = "*")
    public Iterable<Filter> getAllFilters() {
        return filterService.getAllFilters();
    }

    @PostMapping
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public Filter addFilter(@RequestBody Filter filter) {
        LOG.debug("Adding filter: " + filter);
        return filterService.save(filter);
    }

    @PutMapping
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public Filter updateFilter(@RequestBody Filter filter) {
        LOG.debug("Updating filter: " + filter);
        return filterService.save(filter);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public void deleteFilter(@PathVariable Integer id) {
        LOG.debug("Deleting filter: " + id);
        filterService.delete(id);
    }

    @DeleteMapping("/value/{id}")
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public void deleteFilterValue(@PathVariable Integer id) {
        LOG.debug("Deleting filter value: " + id);
        filterService.deleteFilterValue(id);
    }
}
