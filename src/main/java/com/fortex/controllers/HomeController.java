package com.fortex.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/pong")
    @CrossOrigin(origins = "*")
    public String pong() {
        return "pong";
    }
}
