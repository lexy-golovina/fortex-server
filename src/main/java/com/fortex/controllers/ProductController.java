package com.fortex.controllers;

import com.fortex.dto.ProductData;
import com.fortex.entities.Product;
import com.fortex.services.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    private static final Logger LOG = LogManager.getLogger(ProductService.class);

    @Autowired
    private ProductService productService;

    @GetMapping
    @CrossOrigin(origins = "*")
    public Iterable<Product> getAllProducts(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer count) {
        LOG.debug(String.format("Getting (%d) products from page (%d)", page, count));
        return productService.getAllProducts(page, count);
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "*")
    public Product getProductById(@PathVariable long id) {
        return productService.getProductById(id).orElse(new Product()); //TODO: response entity 404
    }

    @PostMapping(consumes = {"multipart/form-data"})
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public Product addProduct(@ModelAttribute ProductData productData) {
        //TODO: move to adapter
        Product product = new Product();
        product.setName(productData.getName());
        product.setDescription(productData.getDescription());
        product.setPrice(productData.getPrice());
        //set properties
        try {
            if (productData.getPicture() != null) {
                product.setPicture(productData.getPicture().getBytes());
            }
        } catch (IOException e) {
            LOG.error("Error with uploading picture: ", e);
        }
        LOG.debug("New product: " + product);
        return productService.save(product);
    }

    @PutMapping
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public Product updateProduct(@RequestBody Product product) {
        LOG.debug("Updated product: " + product);
        return productService.save(product);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public void deleteProduct(@PathVariable Long id) {
        LOG.debug("Deleting product with id: " + id);
        productService.remove(id);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority(ADMIN)")
    @CrossOrigin(origins = "*")
    public void applyFilters(@PathVariable Long id, @RequestParam List<Integer> filterIds) {
        LOG.debug("Adding filters " + filterIds + " to product: " + id);
        productService.applyFilters(id, filterIds);
    }
}
