package com.fortex.repository;

import com.fortex.entities.StoreInfo;
import org.springframework.data.repository.CrudRepository;

public interface StoreInfoRepository extends CrudRepository<StoreInfo, Integer> {
}
