package com.fortex.repository;

import com.fortex.entities.DropDownFilterValue;
import org.springframework.data.repository.CrudRepository;

public interface DropDownFilterRepository extends CrudRepository<DropDownFilterValue, Integer> {

}
