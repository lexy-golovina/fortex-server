package com.fortex.repository;

import com.fortex.entities.Filter;
import org.springframework.data.repository.CrudRepository;

public interface FilterRepository extends CrudRepository<Filter, Integer> {

}
