package com.fortex.services;

import com.fortex.entities.Filter;
import com.fortex.repository.DropDownFilterRepository;
import com.fortex.repository.FilterRepository;
import org.springframework.stereotype.Service;

@Service
public class FilterService {

    private final FilterRepository filterRepository;
    private final DropDownFilterRepository dropDownFilterRepository;

    public FilterService(FilterRepository filterRepository, DropDownFilterRepository dropDownFilterRepository) {
        this.filterRepository = filterRepository;
        this.dropDownFilterRepository = dropDownFilterRepository;
    }

    public Iterable<Filter> getAllFilters() {
        return filterRepository.findAll();
    }

    public Filter save(Filter filter) {
        return filterRepository.save(filter);
    }

    public void delete(Integer filterId) {
        filterRepository.deleteById(filterId);
    }

    public void deleteFilterValue(Integer filterValueId) {
        dropDownFilterRepository.deleteById(filterValueId);
    }
}
