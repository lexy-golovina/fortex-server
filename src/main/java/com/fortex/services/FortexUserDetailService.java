package com.fortex.services;

import com.fortex.entities.FortexUserDetails;
import com.fortex.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class FortexUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    public FortexUserDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepository.findUserByUsername(username)
            .map(FortexUserDetails::new)
            .orElseThrow(() ->  new UsernameNotFoundException("User with username " + username + "not found!"));
    }
}
