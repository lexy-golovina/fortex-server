package com.fortex.services;

import com.fortex.entities.StoreInfo;
import com.fortex.repository.StoreInfoRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StoreService {

    private static final Logger LOG = LogManager.getLogger(StoreService.class);

    private final StoreInfoRepository storeInfoRepository;

    public StoreService(StoreInfoRepository storeInfoRepository) {
        this.storeInfoRepository = storeInfoRepository;
    }

    public Optional<StoreInfo> getStoreInfo(int id) {
        return storeInfoRepository.findById(id);
    }

    public StoreInfo save(StoreInfo storeInfo) {
        LOG.debug("Updated store: " + storeInfo);
        return storeInfoRepository.save(storeInfo);
    }
}
