package com.fortex.services;

import com.fortex.entities.Product;
import com.fortex.repository.DropDownFilterRepository;
import com.fortex.repository.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private static final Logger LOG = LogManager.getLogger(ProductService.class);

    private final ProductRepository productRepository;
    private final DropDownFilterRepository dropDownFilterRepository;

    public ProductService(ProductRepository productRepository, DropDownFilterRepository dropDownFilterRepository) {
        this.productRepository = productRepository;
        this.dropDownFilterRepository = dropDownFilterRepository;
    }

    public Iterable<Product> getAllProducts(Integer page, Integer count) {
        if (shouldPaginate(page, count)) {
            Pageable pageable = PageRequest.of(page, count);
            return productRepository.findAll(pageable)
                    .stream()
                    .peek(this::setNoImagePicture)
                    .collect(Collectors.toList());
        }
        Iterable<Product> all = productRepository.findAll();
        all.spliterator().forEachRemaining(this::setNoImagePicture);
        return all;
    }

    private boolean shouldPaginate(Integer page, Integer count) {
        return page != null && page >= 0 && count != null && count > 0;
    }

    public Optional<Product> getProductById(long productId) {
        Optional<Product> product = productRepository.findById(productId);
        product.ifPresent(this::setNoImagePicture);
        return product;
    }

    public Product save(Product product) {
        setNoImagePicture(product);
        return productRepository.save(product);
    }

    private void setNoImagePicture(Product product) {
        if (product.getPicture() == null) {
            LOG.warn("No picture for product '"+ product.getName() + "'. Setting default picture.");
            product.setPicture(getNoImagePicture());
        }
    }

    private byte[] getNoImagePicture() {
        try {
            return Files.readAllBytes(Paths.get("src", "main", "resources", "static", "images", "noImage.png"));
        } catch (IOException e) {
            LOG.error("Couldn't set default image: ", e);
            return null;
        }
    }

    public void remove(Long id) {
        productRepository.deleteById(id);
    }

    public void applyFilters(Long productId, List<Integer> filterIds) {
        productRepository.findById(productId)
                .ifPresent(product -> applyFilters(filterIds, product));
    }

    private void applyFilters(List<Integer> filterIds, Product product) {
        filterIds.forEach(filterId ->
                dropDownFilterRepository.findById(filterId)
                        .ifPresent(dropDownFilterValue ->
                                product.getProductProperties().add(dropDownFilterValue))
        );

        productRepository.save(product);
    }
}
