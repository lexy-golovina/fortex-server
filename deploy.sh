#!/usr/bin/env bash

docker build -t web .
heroku container:push web -a fortex-server
heroku container:release web -a fortex-server