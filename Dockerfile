FROM maven:3.5-jdk-8-slim

COPY pom.xml .
RUN mvn dependency:go-offline

COPY ./ ./
RUN mvn package -DskipTests=true

CMD java -jar ./target/fortex-server-0.0.1-SNAPSHOT.jar